// import logo from './logo.svg';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import './App.css';
// import HandlingInput from './HandlingInput/HandlingInput';
// import { UseEffect } from './UseEffect/UseEffect';
import CodeMateri from './CodeMateri/CodeMateri';
import CodeMateriCreate from './CodeMateri/CodeMateriCreate';
import { GlobalProvider } from './context/GlobalContext';
import Home from './Home/Home';

function App() {
  return (
    <BrowserRouter>
      <GlobalProvider>
        <Routes>
          <Route path='/' element={<Home />} />
          <Route path='/code-materi' element={<CodeMateri />} />
          <Route path='/create' element={<CodeMateriCreate />} />
          <Route path='/edit/:IdData' element={<CodeMateriCreate />} />
        </Routes>
      </GlobalProvider>
    </BrowserRouter>
  );
}

export default App;
