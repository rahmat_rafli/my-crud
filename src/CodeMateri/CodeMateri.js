import axios from 'axios';
import React, { useState, useEffect, useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import Navbar from '../Components/Navbar';
import { GlobalContext } from '../context/GlobalContext';

const CodeMateri = () => {

    const { state, handleFunction, word, setWord, number, setNumber } = useContext(GlobalContext);

    let {
        data,
        setData,
        input,
        setInput,
        fetchStatus,
        setFetchStatus,
        currentId,
        setCurrentId
    } = state

    let {
        handleDelete,
        handleEdit,
        handleInput,
        handleSubmit
    } = handleFunction


    useEffect(() => {
        //fetch data dengan kondisi
        if (fetchStatus === true) {
            axios.get("https://backendexample.sanbercloud.com/api/contestants")
                .then((res) => {
                    setData([...res.data])
                })
                .catch((error) => {
                })
            setFetchStatus(false)
        }

    }, [fetchStatus, setFetchStatus])

    return (

        <>
            <Navbar />
            <hr />
            <p>{word}</p>
            <p>{number}</p>
            <hr />
            <div>
                <ul>
                    {data !== null && data.map((res) => {
                        return (
                            <>
                                <li>
                                    {res.name} | &nbsp;
                                    <button onClick={handleEdit} value={res.id}>edit</button>
                                    <button onClick={handleDelete} value={res.id}>delete</button>
                                </li>
                            </>
                        )
                    })}
                </ul>
            </div>
        </>
    )

}

export default CodeMateri;
