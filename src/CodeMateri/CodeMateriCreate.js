import axios from 'axios';
import React, { useContext, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import Navbar from '../Components/Navbar';
import { GlobalContext } from '../context/GlobalContext';

import { useParams } from 'react-router-dom';


const CodeMateriCreate = () => {

    const { state, handleFunction } = useContext(GlobalContext);

    let {
        data,
        setData,
        input,
        setInput,
        fetchStatus,
        setFetchStatus,
        currentId,
        setCurrentId
    } = state

    let {
        handleDelete,
        handleEdit,
        handleInput,
        handleSubmit
    } = handleFunction

    let { IdData } = useParams();

    useEffect(() => {
        if (IdData !== undefined) {
            axios.get(`https://backendexample.sanbercloud.com/api/contestants/${IdData}`)
                .then((res) => {
                    let data = res.data
                    setInput(
                        {
                            name: data.name
                        }
                    )
                })
        }
    }, [])


    return (

        <>
            <Navbar />
            <hr />
            <div>
                <p>FORM DATA</p>
                {/* form data */}
                <form onSubmit={handleSubmit}>
                    <span>Nama : </span>
                    <input onChange={handleInput} value={input.name} name='name' />
                    <input type={'submit'} />
                </form>

            </div>
        </>
    )
}

export default CodeMateriCreate;
