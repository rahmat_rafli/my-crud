import React, { useState } from 'react';

const HandlingInput = () => {

    const [input, setInput] = useState(
        {
            name: "",
            hobby: ""
        }
    );

    const handleInput = (event) => {
        let name = event.target.name;
        let value = event.target.value;

        if (name === "name") {
            setInput({ ...input, name: value })
        } else if (name === "hobby") {
            setInput({ ...input, hobby: value })
        }
    }

    return (
        <>
            <p>Belajar handling input: </p>
            <input type="text" name="name" value={input.name} onChange={handleInput} placeholder="Name" />
            <br />
            <input type="text" name="hobby" value={input.hobby} onChange={handleInput} placeholder="Hobby" />
        </>
    );
}

export default HandlingInput;
