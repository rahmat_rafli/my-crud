import React, { useEffect, useState } from 'react';
import axios from 'axios';

export const UseEffect = () => {
    const [data, setData] = useState(null);

    const [input, setInput] = useState(
        {
            name: ""
        }
    )

    const [currentId, setCurrentId] = useState(-1)

    const [fetchStatus, setFetchStatus] = useState(true);

    useEffect(() => {
        if (fetchStatus === true) {
            axios.get("https://backendexample.sanbercloud.com/api/contestants").then((res) => {
                setData([...res.data])
            }).catch((error) => {
                console.log(error);
            })
            setFetchStatus(false);
        }
    }, [fetchStatus, setFetchStatus]);

    const handleInput = (event) => {
        let name = event.target.name;
        let value = event.target.value;

        if (name === 'name') {
            setInput({ ...input, name: value });
        }
    }

    const handleSubmit = (event) => {
        event.preventDefault()

        let { name } = input;

        if (currentId === -1) {
            axios.post('https://backendexample.sanbercloud.com/api/contestants', { name }).then((res) => {
                setFetchStatus(true);
            }).catch((error) => {
                console.log(error);
            });
        } else {
            axios.put(`https://backendexample.sanbercloud.com/api/contestants/${currentId}`, { name }).then((res) => {
                setFetchStatus(true);
            })
        }

        setCurrentId(-1);

        setInput(
            {
                name: ""
            }
        )
    }

    const handleDelete = (event) => {
        let idData = parseInt(event.target.value);

        axios.delete(`https://backendexample.sanbercloud.com/api/contestants/${idData}`).then((res) => {
            setFetchStatus(true);
        })
    }

    const handleEdit = (event) => {
        let idData = parseInt(event.target.value);

        setCurrentId(idData);

        axios.get(`https://backendexample.sanbercloud.com/api/contestants/${idData}`).then((res) => {
            let data = res.data;

            setInput(
                {
                    name: data.name
                }
            )
        });
    }

    return (
        <>
            <div>
                <ul>
                    {data !== null && data.map((res) => {
                        return (
                            <>
                                <li>
                                    {res.name} | &nbsp;
                                    <button onClick={handleEdit} value={res.id}>Edit</button>
                                    <button onClick={handleDelete} value={res.id}>Delete</button>
                                </li>
                            </>
                        )
                    })}
                </ul>
            </div>

            <p>FORM DATA</p>

            <form onSubmit={handleSubmit}>
                <span>Nama: </span>
                <input onChange={handleInput} value={input.name} name='name' />
                <input type={'submit'} />
            </form>
        </>
    )
}
