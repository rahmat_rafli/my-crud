import axios from 'axios';
import React, { createContext, useState } from 'react';
import { useNavigate } from 'react-router-dom';

export const GlobalContext = createContext();

export const GlobalProvider = (props) => {

    let navigate = useNavigate();

    //materi fetching data
    const [data, setData] = useState(null)

    //materi create data
    const [input, setInput] = useState(
        {
            name: ""
        }
    )

    //indikator
    const [fetchStatus, setFetchStatus] = useState(true)

    //indikator
    const [currentId, setCurrentId] = useState(-1)

    const handleDelete = (event) => {
        let idData = parseInt(event.target.value)
        axios.delete(`https://backendexample.sanbercloud.com/api/contestants/${idData}`)
            .then((res) => {
                setFetchStatus(true)
            })

    }

    const handleEdit = (event) => {

        let idData = parseInt(event.target.value)
        setCurrentId(idData)

        navigate(`/edit/${idData}`)


    }


    //handling input
    const handleInput = (event) => {
        let name = event.target.name
        let value = event.target.value
        if (name === "name") {
            setInput({ ...input, name: value })
        }
    }

    //handling submit

    const handleSubmit = (event) => {
        event.preventDefault()
        let {
            name
        } = input
        if (currentId === -1) {
            //create data
            axios.post('https://backendexample.sanbercloud.com/api/contestants', { name })
                .then((res) => {
                    setFetchStatus(true)
                    navigate('/code-materi')
                })
        } else {

            // update data
            axios.put(`https://backendexample.sanbercloud.com/api/contestants/${currentId}`, { name })
                .then((res) => {
                    setFetchStatus(true)
                    navigate('/code-materi')
                })

        }

        //balikin indikator ke -1
        setCurrentId(-1)
        //clear input setelah create data
        setInput(
            {
                name: ""
            }
        )

    }


    let state = {
        data,
        setData,
        input,
        setInput,
        fetchStatus,
        setFetchStatus,
        currentId,
        setCurrentId
    }

    let handleFunction = {
        handleDelete,
        handleEdit,
        handleInput,
        handleSubmit
    }

    const [word, setWord] = useState("Halo, saya dari state 1 dari context");
    const [number, setNumber] = useState("Halo, saya dari state 2 dari context");
    return (
        <GlobalContext.Provider value={
            {
                word,
                setWord,
                number,
                setNumber,
                state,
                handleFunction
            }
        }>
            {props.children}
        </GlobalContext.Provider>
    );
}